FROM alpine:3.7

ARG VERSION
RUN apk --no-cache add \
        --virtual build \
        git \
    && apk --no-cache add \
        bash \
    && git clone https://github.com/sstephenson/bats.git \
    && cd bats \
    && git checkout "v${VERSION}" \
    && ./install.sh /usr/local \
    && cd - \
    && rm -rf bats \
    && apk del build
